/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package utils

import federator.Querying
import com.hp.hpl.jena.graph.{Triple => JTriple}
import utils.Preamble._


object Combinatorics  {

  def pcomponents(cq:CQ)(implicit bound: Int):  List[Set[JTriple]] = {
    val c = cq.toList
    val b = if (bound > c.size) c.size else bound + 1
    if (b < 2) List()
    else{
      (for (i <- 2 until b) yield c.combinations(i).
        filter(x => connectedP(x))).
        flatten.
        map(_.toSet).
        toList
    }
  }

  def connectedTo(t: JTriple, bgp:List[JTriple]) = {
    bgp.exists(t2=> Querying.sharesVarP(t,t2))
  }

  def connectedP(bgp: List[JTriple]): Boolean= bgp match {
    case Nil => true
    case head :: Nil => true
    case head :: tail => {
      val overlap: Option[JTriple] = bgp.find(triple => Querying.sharesVarP(triple, head))
      overlap match {
        case None => false
        case Some(t) => {
          val n = (tail.filterNot(_ == t))
          connectedTo(t, n) && connectedP(n)
        }
      }
    }
  }

  def components(cq: CQ) = {
    implicit val bound = 1000
    pcomponents(cq) ::: (cq map (x => Set(x))).toList
  }

}
