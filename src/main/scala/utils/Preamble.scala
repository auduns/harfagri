package utils

/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import com.hp.hpl.jena.graph.{Triple => JTriple}
import com.hp.hpl.jena.query.ResultSet
import com.hp.hpl.jena.sparql.core.Var
import com.hp.hpl.jena.sparql.engine.binding.Binding

import scala.annotation.tailrec
import scala.collection.JavaConverters._

object Preamble {

  // Global types
  type CQ = Set[JTriple]
  type Solutions = List[Binding]
  type Charter = List[(CQ, Solutions)]

  // Preflight bound
  implicit val bound = 1000

  // Answer set conversions
  implicit def resultSet2Solutions(r: ResultSet) = r.asScala.toVector

  class Sln(val sln: Binding)  {
    def compare(s2: Sln, atts: List[Var]): Int = {
        val bs = atts.map(x => s2.sln.get(x).toString)
        val as = atts.map(x => this.sln.get(x).toString)

        @tailrec
        def aux(as: List[String], bs: List[String]): Int = (as, bs) match {
          case (Nil, Nil) => 0
          case (Nil, _) => -1
          case (_, Nil) => 1
          case (a::as, b::bs) if a < b => -1
          case (a::as, b::bs) if b < a => 1
          case (a::as, b::bs) => aux(as, bs)
        }

        aux(as, bs)
      }

    def < (that: Sln)(atts: List[Var]) = {
      this.compare(that, atts) == -1
   }

   def equals(that: Any)(atts: List[Var]) =   {
     that match {
       case that:Sln => {
         val as = atts.map(x => this.sln.get(x).toString)
         val bs = atts.map(x => that.sln.get(x).toString)
         as == bs
       }
       case _ => this == that
     }
   }

   def <=(that: Sln)(atts: List[Var]) = {
     this.<(that)(atts) || this.equals(that)(atts)
   }
  }
  implicit def solution2Sln(s: Binding) = new Sln(s)

}
