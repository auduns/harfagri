package utils

import utils.Preamble._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


/**
 * Created by aud on 02.03.2015.
 */
object MonadTransformers {

  def f[A, B](f:(A, B) => B)(a: Future[A], b:Future[B]): Future[B] = {
    for {
      x <- a
      y <- b
    } yield f(x, y)
  }

  def collate = f((x: Charter, y: Charter) => x ::: y ) _
  def add = f((x: (CQ, Solutions), y: Charter) => x :: y) _
  def cons(futures: List[Future[(CQ, Solutions)]]): Future[List[(CQ, Solutions)]] = (Future{List[(CQ, Solutions)]()} /: futures) ((x, y)=> add(y,x))
  def concat(futures: List[Future[Charter]]): Future[Charter]= (Future{List[(CQ, Solutions)]()} /: futures) ((x, y) => collate(x,y))


}
