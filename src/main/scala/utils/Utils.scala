/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package utils

import com.hp.hpl.jena.graph.{Node, Triple => JTriple}
import com.hp.hpl.jena.query.{Query, QueryExecutionFactory}
import com.hp.hpl.jena.rdf.model.Model
import analyzer.Extractor

object Utils {

  def powerset[A](s: Seq[A]) =
    Iterator.range(0, 1 << s.length).map(i =>
      Iterator.range(0, s.length).withFilter(j =>
        (i >> j) % 2 == 1
      ).map(s)
    )

  // Query utils

  def getPattern(query:String): Set[JTriple] =  {
    val extractor = new Extractor(query)
    extractor.getBGPs(0)
  }

  def makeExecutableQuery(query:Query, endpoint: Either[String, Model]) = endpoint match {
    case Right(model) => QueryExecutionFactory.create(query, model)
    case Left(string) => QueryExecutionFactory.sparqlService(string, query)
  }

  def encode(pattern: List[Node]) = {
    pattern.map(_.isVariable)
  }

  def elements(triple: JTriple) = List(triple.getSubject, triple.getPredicate, triple.getObject)

  def varPattern = encode _ compose elements _

  def variableP(node: Node): Boolean = node.isVariable

  // Timers
  def average_time[R](runs:Int, code: => R)  = {
    var t: Long = 0
    for (i <- 1 until runs) {
      val t0 = System.currentTimeMillis()
      code
      val t1 = System.currentTimeMillis()
      t = t + (t1 - t0)
    }

    println("Average: " + (t / 20) /1000.0 )
  }

  def time[R](block: => R): R = {
    println("Executing code block:")
    val t0 = System.currentTimeMillis()
    val result = block    // call-by-name
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) + " milliseconds")
    result
  }

}
