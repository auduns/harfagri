/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import com.hp.hpl.jena.graph.{Triple => JTriple, Node}
import Probing._
import scala.Some
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import utils.Combinatorics._
import utils.Preamble._
import federator.Endpoint._

object Distribution {


  /**
   * Computes the even distribution of a conjunctive query (CQ) over a set of endpoints
   * @param pattern A conjunctive query, i.e. a set of Jena triples
   * @param endpoints A list of entities that implement the Queryable typeclass
   * @return A future map that correlates a CQ with the endpoints that cover it
   */
  def even(pattern: CQ, endpoints: List[Endpoint]): Future[Map[CQ, Set[Endpoint]]]  = {
    for (p <- positives(pattern)(endpoints)) yield {
      p.foldLeft(Map.empty[Set[JTriple], Set[Endpoint]]){
        (m, probed) =>  m.get(Set(probed.triple)) match {
          case None => m + (Set(probed.triple) -> Set(probed.endpoint))
          case Some(sources) => m.updated(Set(probed.triple), sources union Set(probed.endpoint) )
        }
      }
    }
  }

  def non_probing_distribution(pattern: CQ, endpoints: List[Endpoint]): Future[Map[CQ, Set[Endpoint]]] = {
    val temp = pattern.toList

    Future{
      temp.foldLeft(Map.empty[Set[JTriple], Set[Endpoint]]) {
        (m, triple) => m + (Set(triple) -> endpoints.toSet)
      }
    }
  }

  /**
   * Computes the prudent distribution of a conjunctive query (CQ) over a set of endpoints
   * @param pattern A conjunctive query, i.e. a set of Jena triples
   * @param endpoints A list of entities that implement the Queryable typeclass
   * @return A future map that correlates a CQ with the endpoints that cover it
   */
  def prudent(pattern: CQ, endpoints: List[Endpoint]): Future[Map[CQ, Set[Endpoint]]] = {
    for (s <- standard(pattern, endpoints)) yield {
      val (one, two) = s partition {case (k, v) => k.size > 1}
      val news = one.flatMap{case (key, value) => for (c <- components(key)) yield (c, value)}
      news ++ two
    }
  }

  /**
   * Computes the standard distribution of a conjunctive query (CQ) over a set of endpoints
   * @param pattern A conjunctive query, i.e. a set of Jena triples
   * @param endpoints A list of entities that implement the Queryable typeclass
   * @return A future map that correlates a CQ with the endpoints that cover it
   */
  def standard(pattern: Set[JTriple], endpoints: List[Endpoint]): Future[Map[Set[JTriple], Set[Endpoint]]] = {
    def fuse(i: (Set[JTriple], Set[Endpoint]), j: (Set[JTriple], Set[Endpoint])) = if (i._2 == j._2) (i._1 union j._1, i._2) else i
    for (e <- even(pattern,endpoints)) yield {
      val (excl, nonexcl) = e partition {case (k, v) => ((v size) == 1)}
      val groups = for (e <- excl) yield (e /: excl) ((i, j) => fuse(i,j))
      (nonexcl ++ groups)

    }
  }




 /* private def connectedP(cq:CQ) = {
    def recurse(bgp: List[JTriple]): Boolean = bgp match {
      case Nil => true
      case head :: rest=> bgp.exists(triple=> Querying.sharesVarP(head,triple)) && recurse(rest)
    }
    recurse(cq.toList)
  }

  private def pairs(cq:CQ) = {
    for {
      f<-cq;
      o<-cq;
      if ((f != o) && Querying.sharesVarP(f,o))
    } yield Set(f,o)

  }

  private def connectedSet(cq: CQ) =  {
    cq.foldLeft(pairs(cq)) {
      (set, element) =>
        set union (set map (sub =>
          if (sub.exists(triple => Querying.sharesVarP(triple, element)))
            sub + element
          else sub))
    }
  }

  protected def pcomponents(cq:CQ):List[CQ] = {
    connectedSet(cq).toList
  }

  private def generalizesP(n1:Node, n2:Node): Boolean = {
    (n1 == n2) || ((n1.isVariable()) && ((!(n2.isVariable())) || (n1 == n2)))
  }

  private def generalizesP(t1:JTriple, t2:JTriple): Boolean = {
    val t = utils.elements(t1).zip(utils.elements(t2))
    (true /: t) ((bool, pair) => (bool && generalizesP(pair._1, pair._2)))
  }

  private def generalizesP(cq1:CQ, cq2:CQ): Boolean = {
    if (cq1.size < cq2.size) false
    else {
      val p = cq1.zip(cq2)
      (true/: p) ((bool, pair)=> generalizesP(pair._1, pair._2))
    }
  }


  private def minima(s:List[Set[JTriple]]) = {
    s.filter(set => !s.exists(set2 => ((set2 subsetOf set) && (set != set2))))
  }*/
}
