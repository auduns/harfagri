/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import com.hp.hpl.jena.graph.{Triple => JTriple}
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global


object Probing {

  case class Probed(triple: JTriple, endpoint: Endpoint, result:Boolean) {
    override def toString = {
      "Probed(" + triple + ", " + endpoint.source + ", " + result + ")"
    }
  }

  def probe(endpoint: Endpoint)(triple: JTriple): () => Probed = () => new Probed(triple, endpoint, endpoint.queryable.asked(triple, endpoint.source, ""))

  /**
   * Probes an endpoint for signature coverage.
   *
   * @param pattern A conjunctive graph pattern
   * @param endpoints  A list of endpoints to check
   * @return A list of Probes representing pairs of triples and Endpoints
   *         such that the triple may be answerable by the Endpoint.
   */
  def positives(pattern: Set[JTriple])(endpoints: List[Endpoint]): Future[List[Probed]] = Future {
        val s = endpoints map (e => probe(e) _)
        val t = s flatMap (f => {pattern map (p => f(p))})
        ((t.par map (f => f())) filter {probed => probed.result == true}).toList
  }
}
