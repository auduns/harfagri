/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import Distribution._
import com.hp.hpl.jena.sparql.engine.binding.{BindingHashMap, Binding}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.hp.hpl.jena.query.{ResultSetFormatter, ResultSetFactory}
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper
import com.hp.hpl.jena.graph.{Triple=> JTriple, NodeFactory}
import analyzer.{JoinAlgorithms, Planner, Extractor}
import com.hp.hpl.jena.sparql.core.Var
import concurrent.{Future, Await}
import concurrent.duration.Duration
import Querying._
import analyzer.Preflights._
import scala.concurrent.ExecutionContext.Implicits.global
import Exceptions.UncoveredSignatureException
import federator.Heuristics._
import utils.Preamble._


import no.ffi.ucq.IRewrite
import no.ffi.stgder.recyclic.Rewriter
import no.ffi.ucq.query.Query




object Federation {


  /**
   * @param query A query string
   * @return * A query vector containin the set of triples and the set of project variables
   * extracted from a query stringg.
   *
   */
  private def qvector(query:String): (List[CQ], List[Var]) =  {
    val extractor = new Extractor(query)
    (extractor.getBGPs, extractor.collectProjections)
  }

  def projects(query:String) = qvector(query)._2
  def cq(query:String) = qvector(query)._1
 // var projects:List[Var] = List()
//  def projects(query:String) = qvector(query)._2


  def solve_plain(query: String, eps: List[Endpoint],
            planner:Planner = new Planner,
            eval:Evaluator = new Evaluator,
            distributor: (Set[JTriple], List[Endpoint]) => Future[Map[Set[JTriple], Set[Endpoint]]] = standard,
            join: (Solutions, Solutions) => Solutions = JoinAlgorithms.pNestedLoopJoin _ ,
            showplan:Boolean = false
             )(implicit bound:Int): Solutions = {
    val (bgps, proj) = qvector(query)

    //println("in solve_plain, porjects: " + proj)
    def answer(bgp:CQ): Future[Charter] = {
      try{
        answer_aux(bgp)
      } catch {
        case e:UncoveredSignatureException => Future{List()}

      }
    }

    def answer_aux(bgp: CQ): Future[Charter] = {
      val distribution = Await.result(distributor(bgp, eps), Duration.Inf)

      try{
        assert(distribution.keySet.reduceLeft(_ union _) == bgp, "The query is not covered by the sources." + "Keyset: " + distribution.keySet)
      } catch {
        case e:UnsupportedOperationException => throw new UncoveredSignatureException("The query is not covered by the sources." + "Keyset: " + distribution.keySet)
      }



      // val planner = new Planner //with MostGeneralOnly
      val plan = planner.partitioned_llp(distribution, proj.toSet)
      if (showplan) analyzer.Trees.pprint(plan)
      eval.eval(plan, join)
    }

    val partials: List[Future[Charter]] = bgps.map(bgp=> answer(bgp))
    val merged: Future[Charter] = (Future{List[(CQ, Solutions)]()} /: partials) ((x, y) =>
      for {
        s <- x
        t <- y
      } yield s ::: t
    )

    val result: Charter = Await.result(merged, Duration.Inf).map {case (cq, sol) => (cq, eval.reduce(sol))}


    postprocess(result, bgps,  proj.toSet)
  }


  def solve(query: String, eps: List[Endpoint],

            onts:List[String] = List(),
            planner:Planner = new Planner,
            eval:Evaluator = new Evaluator,
            distributor: (Set[JTriple], List[Endpoint]) => Future[Map[Set[JTriple], Set[Endpoint]]] = standard,
            join: (Solutions, Solutions) => Solutions = JoinAlgorithms.pNestedLoopJoin _ ,
            showplan:Boolean = false
             )(implicit bound:Int): Solutions = {
    val proj = projects(query)
   // println("Original query:")
   // println(query)

    val ontlist: java.util.List[String] = scala.collection.JavaConversions.seqAsJavaList(onts)
    val rewriter: IRewrite = new Rewriter()
    val q: Query = rewriter.rewrite(query,ontlist)
    //projects = projects ::: (q.getProjectVars map (v => Var.alloc(v.substring(1)))).toList
    val bgps:List[CQ] = new UCQ(q).clauses
   // println("Rewritten clauses:")
    //bgps.foreach(println)

    def answer(bgp:CQ): Future[Charter] = {
      try{
        answer_aux(bgp)
      } catch {
        case e:UncoveredSignatureException => Future{List()}
        case e:AssertionError => Future{List()}
      }
    }

    def answer_aux(bgp: CQ): Future[Charter] = {
      val distribution = Await.result(distributor(bgp, eps), Duration.Inf)

      try {
        assert(distribution.keySet.reduceLeft(_ union _) == bgp, "The query is not covered by the sources." + "Keyset: " + distribution.keySet)
      } catch {
        case e: UnsupportedOperationException => throw new UncoveredSignatureException("The query is not covered by the sources." + "Keyset: " + distribution.keySet)
      }
      val plan = planner.partitioned_llp(distribution, proj.toSet)
      if (showplan) analyzer.Trees.pprint(plan)
      eval.eval(plan, join)
    }


    val partials: List[Future[Charter]] = bgps.map(bgp=> answer(bgp))
    val merged: Future[Charter] = (Future{List[(CQ, Solutions)]()} /: partials) ((x, y) =>
      for {
        s <- x
        t <- y
      } yield s ::: t
    )

    val result: Charter = Await.result(merged, Duration.Inf).map {case (cq, sol) => (cq, eval.reduce(sol))}
 /*   println("Charter : " + result)
    println(projects)*/
    postprocess(result, bgps,  proj.toSet)
  }

  /**
   *
   * @param solution A list of labelleled bindings
   * @param bgps A UCQ in the form of a list of sets of Jena triples
   * @param proj Project variables
   * @return  A list of bindings corresponding of the union of all the bindings labelled by the bgp
   */
  def postprocess(solution: Charter, bgps: List[CQ], proj:Set[Var]) = {
    //println("in postprocess, projects:" + proj)
    val admissible: Charter = solution.filter{case (cq, s) => (bgps contains cq) }
    val acceptable = for ((cq, s) <- admissible) yield (cq, s.filter(x=> proj subsetOf x.vars().asScala.toSet))

    val answerset = (List[Binding]() /: acceptable) ((a, b) => (b._2 ::: a).toSet.toList)
    answerset
    //Heuristics.truncate(answerset, proj)
  }

  /**
   * Display a list of Bindings in the form of a Jena ResultSet
   * @param solutions A list of Jena Bindings
   * @param projections The variables to project
   */
  def display(solutions: List[Binding], projections: List[Var]) {
    val qiter = solutions.toIterator.asJava
    solutions match {
      case Nil =>  ResultSetFactory.create(new QueryIterPlainWrapper(qiter), projections map {v => v.getVarName()})
      case head::tail =>  {
        val saturated: List[Binding] = for (binding <- solutions) yield  {
          val completed = new BindingHashMap()
          projections map { proj =>
            val node = binding.get(proj)
             if(node != null) completed.add(proj, node)
             else {completed.add(proj, NodeFactory.createAnon())}
          }
          completed.asInstanceOf[Binding]
        }
        ResultSetFactory.create(new QueryIterPlainWrapper(saturated.toIterator.asJava), projections map {v => v.getVarName()})
      }
    }
    ResultSetFormatter.out(ResultSetFactory.create(new QueryIterPlainWrapper(qiter), projections map {v => v.getVarName()}))
  }

}
