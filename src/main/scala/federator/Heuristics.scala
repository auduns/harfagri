/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import com.hp.hpl.jena.sparql.core.Var
import scala.collection.JavaConverters._
import com.hp.hpl.jena.sparql.engine.binding.{BindingFactory, Binding}
import Querying._
import utils.Preamble._
import scala.collection.JavaConversions
import scala.collection.JavaConverters


object Heuristics {
  /**
   *
   * @param b A Jena Binding
   * @return  A truncated Jena Binding without blank nodes in it
   */
  private def omit_blanks(b:Binding) = {
    val newbinding = BindingFactory.create()
    vars(b).foreach(v => {
      if (!(b.get(v).isBlank)) newbinding.add(v, b.get(v))})
    newbinding
  }


  def truncate(s:Solutions, live: Set[Var]): Solutions = {
    if (s.isEmpty) List()

    else{
      def has(b:Binding) = (live intersect b.vars().asScala.toList.toSet).isEmpty
      (s.filterNot(sol => has(sol)) map (binding => {
            val narrowed = BindingFactory.create()
            (live intersect binding.vars().asScala.toList.toSet).foreach(v => narrowed.add(v, binding.get(v)))
            narrowed
          } )).toSet.toList
      }

    }


  /*def truncate(s:Solutions, proj: Set[Var]): Solutions = {
    if (s.isEmpty) List()
    else{
      val has = proj intersect s.head.vars().asScala.toList.toSet
      //val reduced = s.filter(binding => (proj forall (v => binding.contains(v))) )
      if (has.isEmpty) List()
      else{
        (s map (binding => {
          val narrowed = BindingFactory.create()
          has.foreach(v => narrowed.add(v, binding.get(v)))
          narrowed
        }
          )).toSet.toList
      }

    }

  }*/

  /**
   *
   * @param s A list of Jena Bindings
   * @return  A list of truncated Jena Bindings without blank nodes in them
   */
  private def omit_blanks(s: List[Binding]): List[Binding] = {
    s map (b => omit_blanks(b))
  }

  def prune(s: Solutions, vars: List[Var]): Solutions = {
    s.filterNot(sol => (sol.vars().asScala.toList).filter(vars.toSet).exists(v => sol.get(v).isBlank))
  }

  def iso_reduction(s:Solutions): Solutions = {
    def aux(s:Solutions, simile: List[Binding], acc: Solutions): Solutions = {
      s match {
        case Nil => acc
        case head::tail =>
          val b = omit_blanks(head)
          if (simile contains b )
            aux(tail, simile, acc)
          else
            aux(tail, b :: simile, head :: acc )
      }
    }
    aux(s, List(), List())
  }

  trait Reduction{
    def reduce(s:Solutions): Solutions
  }

  trait NoReduction extends Reduction {
    override def reduce(s:Solutions) = s
  }

  trait IsoReduction extends Reduction {
    override def reduce(s:Solutions) = iso_reduction(s)
  }
}
