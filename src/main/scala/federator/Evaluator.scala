/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import concurrent.duration.Duration


import com.hp.hpl.jena.sparql.engine.binding.Binding
import com.hp.hpl.jena.graph.Node
import analyzer.JoinAlgorithms
import analyzer.Trees.Tree

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import Querying._
import scala.Predef._
import analyzer.Trees.Join
import analyzer.Trees.BGP
import Heuristics._
import utils.Preamble._
import utils.MonadTransformers._
import scala.collection.JavaConverters._


class Evaluator extends NoReduction {



  private def combine(join: (Solutions, Solutions) => Solutions) = f((x: Charter, y: Charter) => c(x,y, join)) _


  /**
   * Processes the preflights associated with the endpoints comprised by
   * a leaf node in an execution plan.
   *
   * @param bgp A leaf in an execution plan
   * @return A future containing a list of labelled bindings (CQ, Solutions)
   */
  private def process_preflights(bgp:BGP, join: (Solutions, Solutions) => Solutions): Future[Charter] = {
    def process_preflight(endpoint: Endpoint, ucq: List[CQ]): Future[Charter]  = {
      val answers: List[Future[(CQ, Solutions)]] = for (cq <- ucq) yield {
        endpoint.queryable.select(cq, endpoint.source, variables(cq).toList, (Set(), Nil))
      }
      val t = cons(answers)
      t
    }

    val endpoints = bgp.preflights.keys.toList.filterNot(key => bgp.preflights.get(key).get == Nil)
    val preflights: Future[Charter] = endpoints match {
      case Nil => Future{Nil}
      case _ => {
        val all: List[Future[Charter]]  = (endpoints map
          (endpoint => {
            val ucq = bgp.preflights.get(endpoint).get
            process_preflight(endpoint, ucq)
          }
            )
          )
        all.reduceLeft(combine(join) )
      }
    }
    preflights
  }


  /**
   * Implements the central combine algorithm:
   * Labelled bindings are combined by folding the following operation over the union
   * of the two charters that are passed in as arguments:
   *   - Labelled bindings with non-overlapping keys: (union of keys, join of results)
   *   - Labelled bindings with identical keys: (one of the keys, union of the results)
   *   - Labelled bindings with overlapping keys: Return the left labelled binding
   *
   * @param c1 A list of labelled bindings (CQ, Solutions)
   * @param c2 A list of labelled bindings (CQ, Solutions)
   * @param join A join algorithm
   * @return A list of labelled bindings (CQ, Solutions)
   */
  private def c(c1: Charter, c2:Charter, join: (Solutions, Solutions) => Solutions): Charter = {
    def splice(join: (Solutions, Solutions) => Solutions)(c1: (CQ, Solutions), c2:(CQ, Solutions)) = {
      val (cq1, sol1) = c1
      val (cq2, sol2) = c2
      if (cq1 == cq2) (cq1, (sol1 ::: sol2).toSet.toList)
      else if ((cq1 intersect cq2).isEmpty) {
        val j = join(sol1, sol2)
        (cq1 union cq2, j)
      }
      else {
        c1
      }
    }

    val all = (c1 ::: c2).toSet.toList
    val r = all.map {
        lbind => {
        (lbind /: all) (splice(join) _)
      }
    }.toSet.toList
    val result = r.filterNot(x=> x._2 == Nil)
    result.map{case (cq, sol) => (cq, reduce(sol))}
  }

  private def make_solutions(data: Charter): Solutions = {
    (List[Binding]() /: data)((a, b) => (a ::: b._2).toSet.toList)
  }

  /**
   * Process the main pattern held by a leaf node (BGP) in an execution plan
   *
   * @param bgp A leaf node in an execution plan
   * @param data A labelled binding that acts as a set of constraints
   * @return A future labelled binding
   */
  private def process_principal(bgp:BGP, data: (CQ, Solutions) = (Set(), List())): Future[Charter] = {
    //data._2.foreach(x=> println("(" + data._1 + ", " + x + ", " + x.vars().asScala.toList + ")"))

    val (cq, sol) = data
      val pruned: Solutions = {
        val temp = sol// prune(sol, bgp.joins.toList)
        // If pruning erases all passed-in values, then just pass-in one value to make
        // sure the query fails
        if (!sol.isEmpty && temp.isEmpty)
          List(sol.head)
        else temp  //joins.toList)
      }
      val principals: List[Future[(CQ, Solutions)]] = (
        bgp.endpoints map (
          endpoint => endpoint.queryable.select(bgp.triples, endpoint.source, bgp.live.toList, (cq, pruned)))
        )

      val x = cons(principals)
      //Await.result(x,Duration.Inf).foreach(println)
      x
  }

  /**
   *
   * @param bgp A execution plan in as a no.ffi.hourglass.analyzer.Trees.BGP; a lea of an execution plan
   * @return A future list of labelled bindings
   */
  private def process_type1(bgp:BGP, join: (Solutions, Solutions) => Solutions): Future[Charter] = {
    val principal: Future[Charter] = process_principal(bgp, (Set(), Nil))
    val preflights: Future[Charter] =  process_preflights(bgp, join)
    val r = collate(combine(join)(principal, preflights), principal)
    r.map(charter => charter.map {case (cq, sol) => (cq, reduce(sol))})
  }

  /**
   *
   * @param bgp A execution plan in as a no.ffi.hourglass.analyzer.Trees.BGP; a lea of an execution plan
   * @param received A future list of labelled bindings
   * @return A future list of labelled bindings
   */
  private def process_type2(bgp:BGP, received: Future[Charter], join: (Solutions, Solutions) => Solutions): Future[Charter] = {
    val preflights: Future[Charter] = process_preflights(bgp, join)
    val combo: Future[Charter] = combine(join)(received, preflights)
    val partition: Future[(Charter, Charter)] = combo.map(c => c.partition{case (cq, sol) => (cq intersect bgp.triples).isEmpty} )
    val main_results: Future[Charter] = partition.map{
      case (joinables: Charter, _) =>
        if (joinables == Nil) List(process_principal(bgp))
        else {
          val pruned = joinables.map{case (cq, sol) => (cq, prune(sol,((bgp.joins.toList) ::: variables(bgp.triples))))}
          pruned.map((jn: (CQ, Solutions)) => process_principal(bgp, jn))
        }
    }.flatMap(x => concat(x))
    val r = for {
      m<- main_results
      (_, props) <- partition
      r<-received
    } yield (m ::: props)
    r.map(charter => charter.map {case (cq, sol) => (cq, reduce(sol))})

  }

  /**
   * Precondition: Execution plans are left-leaning.
   *
   * @param plan An execution plan in as a no.ffi.hourglass.analyzer.Trees.Tree
   * @param join A join algorithm
   * @return A future of labelled of the form bindings (Pattern, Solution)
   */
  def eval(plan: Tree, join: (Solutions, Solutions) => Solutions = JoinAlgorithms.pNestedLoopJoin _): Future[Charter] = {

    def recurse(branch: Tree, charter: Charter): Future[Charter] = {
      (branch: @unchecked) match {
        case branch:BGP =>  {
          process_type1(branch, join)
        }
        case tree@Join(a:BGP, b:BGP) => {
          val r1 = process_type1(a, join)
          val r2 =  process_type1(b, join)
          val cg = combine(join)(r1,r2)
          cg
        }
        case  tree@Join(a:Join, b:BGP) => {
          val p = recurse(a, charter)
          process_type2(b,p, join)
        }
      }
    }
    recurse(plan, List())
  }
}
