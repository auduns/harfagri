/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import com.hp.hpl.jena.query.{Query, QueryExecutionFactory, QueryExecution}
import com.hp.hpl.jena.sparql.core.Var
import com.hp.hpl.jena.graph.{Triple => JTriple, Node}
import scala.collection.JavaConverters._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import harfagri.federator.Substitution._
import com.hp.hpl.jena.rdf.model.Model
import utils.Preamble._


/**
 * A type class for entities that can be queried with SPARQL
 *
 * @tparam T A queryable endpoint of some kind
 */
trait Queryable[T] {
  def ask(pattern: Set[JTriple], endpoint: T, filter: String): QueryExecution

  /**
   *
   * @param triple A Jena Triple
   * @param endpoint A queryable endpoint of some kind
   * @return An ask QueryExecution object
   */
  def ask(triple: JTriple,  endpoint: T, filter: String): QueryExecution = ask(Set(triple), endpoint, filter)

  /**
   *
   * @param pattern  A set oj Jena triples
   * @param endpoint A queryable endpoint of some kind
   * @return true or false
   */
  def asked(pattern: CQ, endpoint: T, filter:String): Boolean = ask(pattern, endpoint, filter) execAsk

  /**
   *
   * @param triple  A Jena triple
   * @param endpoint A queryable endpoint of some kind
   * @return true or false
   */
  def asked(triple: JTriple, endpoint: T, filter: String): Boolean = ask(triple, endpoint, filter) execAsk

  /**
   *
   * @param cq A conjunctive query; a set of Jena triples
   * @param endpoint A queryable endpoint of some kind
   * @param vars A list of variables to project
   * @param values A labelled binding used as additional data
   * @return A future labelled binding obtained by posing 'CQ' against 'endpoint' with 'values' as additional data and projecting out 'vars'.
   */
  def select(cq: CQ, endpoint: T, vars:List[Var],  values: (CQ, Solutions) = (Set(), List()), limit: Option[Int] = None): Future[(CQ, Solutions)]
}

object Queryable{
  import Querying._

  /**
   * Adds as set of VALUEs to a query
   *
   * @param q A Jena Query
   * @param bs A list of jena Bindings
   * @return A Jena Query Q
   */
  private def inform(q:Query, bs: Solutions): Query = {
    if (!bs.isEmpty) {
      val vs: List[Var] = (List[Var]() /: (bs map (x => vars(x))))(_ ::: _).toSet.toList
     // println(bs.head. + " " + bs.isEmpty)
      vs.foreach(v => q.addResultVar(v))
      q.setValuesDataBlock(vs.asJava, replace_blanks(bs).asJava)
    }

    q
    }

  /**
   * Type class implementation for in-memory models
   */
  implicit object InMemoryModel extends Queryable[Model] {
    override def ask(pattern: Set[JTriple], endpoint: Model, filter:String = "") = {
      val q = toAsk(pattern, filter)
      QueryExecutionFactory.create(q,endpoint)
    }

    override def select(pattern: CQ, endpoint: Model, live_vars:List[Var], bs: (CQ, Solutions)  = (Set(), List()), limit: Option[Int] = None) = Future {
      val (cq, sol) = bs
      val query = inform(toSelect(List(pattern), (s:List[CQ]) => live_vars), sol)
      limit match{
        case None =>
        case Some(int) => query.setLimit(int)
      }
      val exec = QueryExecutionFactory.create(query, endpoint)
      val result = sol2Bind(exec.execSelect())
      (pattern union cq, result )

    }
  }

  /**
   * Type class implementation for remote SPARQL endpoints
   */
  implicit object RemoteSource extends Queryable[String] {
    override def ask(pattern: Set[JTriple], endpoint: String, filter: String = "") = {
      val q = toAsk(pattern, filter)
      QueryExecutionFactory.sparqlService(endpoint,q)
    }

    override def select(pattern: CQ, endpoint: String, live_vars:List[Var], bs: (CQ, Solutions)  = (Set(), List()), limit: Option[Int]) = Future {
      //bs._2.foreach(x=> println("(" + bs._1 + ", " + x + ", " + x.vars.asScala.toList + ")"))
      val (cq, sol) = bs
      val t =  Heuristics.truncate(sol, live_vars.toSet)
      val query = inform(toSelect(List(pattern), (s:List[CQ]) => live_vars), t)// truncate(sol, live_vars.toSet))//
      limit match{
        case None =>
        case Some(int) => query.setLimit(int)
      }
      println(query)
      val exec = QueryExecutionFactory.sparqlService(endpoint, query)
      val result = sol2Bind(exec.execSelect())
      (pattern union cq, result )

    }
  }

}

