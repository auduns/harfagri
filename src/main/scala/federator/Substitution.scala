/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package harfagri.federator

import scalaz._
import scalaz.Scalaz._
import com.hp.hpl.jena.sparql.engine.binding.{BindingFactory, Binding}
import scala.collection.JavaConversions._
import com.hp.hpl.jena.graph.{Triple => JTriple, NodeFactory, Node}
import scala.Some



object Substitution {

  case class SubState(sub: Map[Node, Node], count: Int)

  def substitution_step(node: Node): State[SubState, Unit] = modify { prev:SubState =>

    if (node.isBlank) {
      val replacement = prev.sub.get(node)
      replacement match {
        case None => {
          val placeholder = NodeFactory.createURI("http://placeholder#" + node.hashCode())
          SubState(prev.sub + (node -> placeholder), prev.count + 1)
        }
        case Some(num) => prev
      }
    } else prev

  }

  def sub_from_binding(binding:Binding):State[SubState, Map[Node, Node]] = {
    val nodes = binding.vars().toList map (v => binding.get(v))
    for {
      _ <- nodes traverseS substitution_step
      s <- gets {s:SubState => s.sub}
    } yield s
  }

  def sub_from_solutions(solutions: List[Binding]): State[SubState, Map[Node, Node]] = {
    for {
      _ <- solutions traverseS sub_from_binding
      s <- gets {s:SubState => s.sub}
    } yield s
  }

  def make_substitution(solutions: List[Binding], state:SubState): (SubState, Map[Node, Node]) = {
    sub_from_solutions(solutions).run(state)
  }


  def replace_blanks(solutions: List[Binding]): List[Binding] = {
    def newb(b: Binding,  state:SubState): Binding = {
      val nb = BindingFactory.create()
      b.vars().toList.foreach(v => {
        val n = b.get(v)
        if (n.isBlank) {
          nb.add(v, state.sub.get(n).get)
        } else{
          nb.add(v, n)
        }
      })
      nb
    }

    val (st, sub) = make_substitution(solutions, SubState(Map.empty[Node, Node], 0))
    (solutions map (b => newb(b, st)))

  }
}
