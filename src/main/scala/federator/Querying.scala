/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package federator

import com.hp.hpl.jena.query.{ResultSet, QueryExecutionFactory, Query}
import com.hp.hpl.jena.sparql.syntax.ElementTriplesBlock
import com.hp.hpl.jena.sparql.engine.binding.{BindingFactory, Binding}
import com.hp.hpl.jena.sparql.core.Var
import com.hp.hpl.jena.graph.{Triple => JTriple, Node}
import scala.collection.JavaConverters._
import utils.Preamble._

object Querying  {

  /**
   *
   * @param binding A Jena Binding
   * @return A list of jena Vars contained in the binding.
   */
  def vars(binding:Binding): List[Var] = binding.vars.asScala.toList



  /**
   * Utility method
   *
   * @param pattern A set of Jena triples
   * @return  A Jena ElementTriplesBlock
   */
  def toBlock(pattern: CQ): ElementTriplesBlock = {
    val bgp: ElementTriplesBlock = new ElementTriplesBlock()
    pattern map (p => (bgp addTriple p))
    bgp
  }

  /**
   *
   * @param pattern A set of Jena triples
   * @return A Jena Query
   */
  def toQuery(pattern: CQ): Query = {
    val query: Query = new Query()
    query.setQueryPattern(toBlock(pattern))
    query setDistinct true
    query
  }

  /**
   *
   * @param t A Jena triple
   * @return The variables in t in the form of a list of Jena Nodes
   */
  def variables(t: JTriple): List[Var] = elements(t).filter(_.isVariable).map(_.asInstanceOf[Var])

  /**
   *
   * @param t A set of Jena triples
   * @return The set of variables in t, in the form of a list of Jena Nodes
   */
  def variables(t: CQ): List[Var] = t.flatMap(e=> elements(e)).filter(_.isVariable).toList.map(_.asInstanceOf[Var])

  /**
   *
   * @param t A list of sets of Jena triples
   * @return The set of variables in t, in the form of a list of Jena Nodes
   */
  def variables(t: List[CQ]): List[Var] = t.flatMap(a=> variables(a))

  /**
   *
   * @param ucq A list of CQs, i.e. of sets of Jena triples
   * @param vars The variables to be projected
   * @return A Jena Query
   */
  def toQuery(ucq:List[CQ], vars: List[Node], filter:String = ""):Query = {
    val initial_string =  "SELECT " + vars.toSet.toList.mkString(" ")  + " WHERE {\n"
    def repr(t:JTriple) =  (elements(t) map {
      y:Node => if (y.isURI) "<" + y.toString() +">" else y.toString()
    }).mkString(" ")

    def block(s:Set[JTriple]) = (s map (x => repr(x))).mkString(".\n") + "\n"
    def agg(s:List[Set[JTriple]]) = (s map(x=> block(x))).map(x => "{" + x + "}").mkString("UNION\n")

    (ucq: @unchecked) match {
      case head :: Nil => {
        val s = initial_string + block(head) + " " + filter + "}"
        QueryExecutionFactory.create(initial_string + block(head) + " " + filter + "}").getQuery//toQuery(head, filter)

      }

      case head :: next :: tail => QueryExecutionFactory.create(initial_string + agg(ucq) + " " + filter + "}").getQuery
    }
  }

  /**
   *
   * @param triple  A Jena triple
   * @return  A list of subject, predicate and object
   */
  def elements(triple: JTriple) = List(triple.getSubject, triple.getPredicate, triple.getObject)

  /**
   *
   * @param ucq A list of sets of Jena triples
   * @param projector A function that determines which variables are to be projected
   * @return A Jena Query
   */
  def toSelect(ucq: List[CQ], projector: List[CQ] => List[Node] = variables): Query = {
    val live = projector(ucq)
    val query = toQuery(ucq, live)
    live foreach (x => query.addResultVar(x))
    (query setQuerySelectType)
    query
  }

  /**
   *
   * @param pattern A Set of Jena triples
   * @return A Jena Query of ASK type
   */
  def toAsk(pattern: CQ, filter:String = ""): Query = {
    val query = toQuery(List(pattern), variables(pattern), filter)
    (query setQueryAskType)

    query }


  /**
   *
   * @param s A list of Jena Bindings
   * @param proj A list of variables to project s onto
   * @return A set of truncated bindings
   */
  def project(s:Solutions, proj: Set[Var]): Solutions = {
    if (s.isEmpty) List()
    else{
      val has = proj intersect s.head.vars().asScala.toList.toSet
      val reduced = s.filter(binding => (proj forall (v => binding.contains(v))) )
      val result = (reduced map (binding => {
        val narrowed = BindingFactory.create()
        proj.foreach(v => narrowed.add(v, binding.get(v)))
        narrowed
      }
        )).toSet.toList
      Federation.display(result, proj.toList)
      result
    }

  }


  /**
   *
   * @param t1 A Jena triple
   * @param t2 A Jena triple
   * @return true if t1 and t1 share a variable, false otherwise
   */
  def sharesVarP(t1: JTriple, t2: JTriple) = (variables(t1) intersect variables(t2)).size != 0

  /**
   * Convert a Jena ResultSet to a list of jena Bindings
   * @param r a Jena ResultSet
   * @return A list of the Bindings in r
   */
  def sol2Bind(r: ResultSet): List[Binding] = {
    var bindings = List[Binding]()
    while(r.hasNext) {
      bindings = r.nextBinding::bindings
    }
    bindings
  }

}


