/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package analyzer

import com.hp.hpl.jena.sparql.algebra._
import com.hp.hpl.jena.sparql.algebra.op._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.hp.hpl.jena.graph.{Triple => JTriple}
import com.hp.hpl.jena.sparql.sse.SSE
import com.hp.hpl.jena.sparql.core.Var

import com.hp.hpl.jena.query._

class Extractor(qry: String){

  val collector: JenaQueryVisitor = new JenaQueryVisitor
  val query: Query = QueryFactory.create(qry) ;

  val op = Algebra.compile(query)
  OpWalker.walk(op, collector)


  class JenaQueryVisitor extends OpVisitorBase{
    private var bgps: List[OpBGP] = List()
    private var proj: OpProject = _ ;

    override def visit(proj: OpProject) = {
      this.proj = proj
      super.visit(proj)
    }

    override def visit(bgp: OpBGP) = {
      val newstr = bgp.getPattern.toString.replaceAll("\\?\\?", "\\?")
      this.bgps = SSE.parseOp("(bgp " + newstr +")").asInstanceOf[OpBGP] :: bgps
    }

    def getBGPs = bgps
    def getProjections: List[Var]= query.getProjectVars.asScala.toList

  }

  def getBGPs: List[Set[JTriple]] = collector.getBGPs map (_.getPattern.getList.toList.toSet )
  def collectProjections = collector.getProjections
}


