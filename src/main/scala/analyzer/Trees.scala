/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package analyzer

import com.hp.hpl.jena.graph.{Triple=> JTriple, Node}
import federator.{Endpoint}
import com.hp.hpl.jena.sparql.core.Var
import federator.Querying._
import utils.Preamble._


object Trees {

  sealed abstract class Tree {
    private var _projects: Set[Var] = Set()
    private var _live: Set[Var] = Set.empty
    private var _joins: Set[Var] = Set()
    private var _parent:Option[Tree] = None
    def live: Set[Var] = _live
    def live_=(l: Set[Var]) = _live = l
    def triples: Set[JTriple]
    def projects = _projects
    def projects_=(s:Set[Var]) = _projects = s
    def parent: Option[Tree] = None
    def parent_=(t:Tree) = _parent = Some(t)
    def joins = _joins
    def joins_=(j:Set[Var]) = _joins = j
  }

  case class BGP(triples: Set[JTriple], endpoints: List[Endpoint],  preflights: Map[Endpoint, List[CQ]]) extends Tree

  case class Join(private var _left: Tree, right: Tree) extends Tree {
    def left_=(t:Tree) = _left = t
    def left = _left
    left.parent = this; right.parent = this
    def triples = left.triples union right.triples
    def rootP: Boolean = this.parent == None
  }

  def pprint(tree: Tree, level: Int = 1, indent: String = "-"): Unit = tree match {
    case b@BGP(triples,endpoints,  map) => {
      println("|"+ (indent * level * 3) + "BGP of: ")
      println("|"+  (" " * level * 3) + "\tTriples:" )
      println("|"+  (" " * level * 3) + "\t live_vars:" + b.live )
      println("|"+  (" " * level * 3) + "\t joins:" + b.joins )

      for (t <- triples) println("|"+ (" " * level * 3) + "\t\t" + t)
      println("|"+ (" " * level * 3) + "\tEndpoints:" )
      for (e <- b.endpoints) println("|"+ (" " * level * 3) + "\t\t" + e.source)
    }

    case t@Join(left, right) =>  {
      println("|" + (indent * level * 3) + "Join of: ")
      println("|" + (indent * level * 3) + "live_vars: " + t.live )

      println("|"+  (indent * level * 3) + "joins:" + t.joins )

      // println("|" + (indent * level * 3) + "triples: " + t.triples )
      pprint(left, level+1)
      pprint(right, level+1)
    }
  }

  def collect_vars(tree:Tree, acc:List[Node] = List()): List[Node] = tree match {
    case t:BGP=> acc ::: variables(t.triples).toList
    case Join(left,right) => (collect_vars(left) ::: collect_vars(right)).toSet.toList
  }

  def height(tree: Tree): Int = {
    tree match {
      case tree: BGP => 0
      case Join(left,right) => scala.math.max(height(left), height(right)) + 1
    }
  }
}
