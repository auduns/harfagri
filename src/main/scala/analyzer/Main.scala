
package analyzer

import concurrent.{Future, Await}
import concurrent.duration.Duration
import com.hp.hpl.jena.sparql.core.Var
import analyzer.Preflights._
import federator.{Distribution, Evaluator, Endpoint}
import federator.Federation._


object Main extends App {

  val common_prefixes =
    "PREFIX : <http://no.ffi/harfagri#>"+
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"+
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"+
    "PREFIX orbat: <urn:int:nato:majiic:organization:orbat>\n"+
    "PREFIX ccirm: <urn:int:nato:majiic:ccirm:ccirmcommontypes>\n"+
    "PREFIX isrc: <urn:int:nato:majiic:common:isrcommon>\n"+
    "PREFIX sds: <urn:int:nato:majiic:ccirm:sds>\n"+
    "PREFIX request: <urn:int:nato:majiic:ccirm:request>\n"

  val query = common_prefixes + "SELECT DISTINCT ?unit ?Nation{"+
    "?unit isrc:UnitIdentifier ?unitIdentifier."+
    "?unit isrc:Nation ?Nation ."+
    "?c2 orbat:UnitNode ?unit." +
    "}"


  val ontquery = common_prefixes + "SELECT DISTINCT ?unit ?m {"+
    "?unit isrc:UnitIdentifier ?unitIdentifier."+
    "?unit :Municipality ?m ."+
    "?c2 orbat:UnitNode ?unit." +
    "}"

  val query3 = common_prefixes + "SELECT DISTINCT ?unit ?Nation {"+
    "?unit isrc:UnitIdentifier ?unitIdentifier."+
    "?unit isrc:Nation ?Nation ."+
    "?parent orbat:hasC2Of ?c2." +
    "?c2 orbat:UnitNode ?unit. "+
    "?parent isrc:UnitIdentifier ?ParentUnitIdentifier."+
    "?parent isrc:Nation ?ParentNation.}"

  val query2 =  "SELECT ?x ?o WHERE{ ?s <http://fileA/p1> ?x. "+
    "?s <http://fileA/p2> ?y."+
    //"?t <http://fileA/p2> ?o."+
    "?x ?p ?o.}"


  val query4 =  "SELECT ?s ?x WHERE{ ?s <http://prop/p1> ?x. "+
    "?s <http://prop/p2> ?y.}"


  val query5 = "SELECT ?district ?population  WHERE {" +
    "?s <http://vocab.lenka.no/hvor#bydel> ?district .  " +
    "?district <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://dbpedia.org/ontology/Settlement>. "+
    "?district <http://dbpedia.org/ontology/isPartOf> <http://dbpedia.org/resource/Oslo> ." +
    "?district <http://dbpedia.org/property/populationTotal> ?population.}"

  val query6 = "SELECT ?i2 WHERE {" +
    "?a <http://prop/p1> ?i1." +
    "?a <http://prop/p2> ?i2." +
    "?i1 <http://prop/p1> ?y." +
    "?i2 <http://prop/p2> ?y.}"

  val query7 = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> PREFIX tennis: <http://www.tennis.com/ontology#>"+
    "SELECT ?name ?year  WHERE {"+
    "?athlete a tennis:TennisPlayer."+
    "?athlete foaf:name ?name."+
    "?athlete tennis:wins ?x. " +
    "?x tennis:event ?event."+
    "?x tennis:year ?year. "+
    "?event a  tennis:GrandSlamTournament."+
    "}"

  val query8 =
    "PREFIX : <http://www.example.org#>" +
    "SELECT  ?nat WHERE {{?s ?p 'GBR'.} UNION {?s  <urn:int:nato:majiic:common:isrcommonNation> ?nat.}}"

  val query9 = "SELECT  ?s WHERE {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o.}"

  val rew = "PREFIX  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
  "SELECT ?mission ?unit ?lat ?long"+
  "WHERE  {	{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event." +
  "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
    "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat." +
    "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
    "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
    "UNION"+
     "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
      "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
      "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}" +
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
      "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
      "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
      "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
    "UNION" +
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
      "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
      "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
        "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}"+
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
        "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
    "UNION" +
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
      "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
      "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
      "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat.}"+
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
      "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
    "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
      "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
      "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
      "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
        "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
        "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>.}"+
      "UNION"+
    "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
        "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
        "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
        "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
        "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
        "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>.}"+
        "UNION"+
      "    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos.          "+
      "    ?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event.                                          "+
     "     ?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categoies/F38056CC-753B-48C5-951C-88850CE91DB1>. "+
     "     ?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
     "     ?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat.                                                             "+
     "       ?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}" +
        "UNION"+
     "{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
          "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}"+
         "UNION 						   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
          "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
        "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
        "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
          "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
        "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
          "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
        "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
          "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
        "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
          "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
        "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
        "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
          "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
        "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
          "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
          "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
        "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
          "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
        "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
          "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
          "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
          "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>.}"+
          "UNION 	       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
              "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
          "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
              "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long.}"+
          "UNION 								 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
          "UNION 								     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
           "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
          "UNION 								     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}"+
          "UNION 						   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long."+
              "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long."+
              "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long."+
              "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat.}"+
          "UNION 							      {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long."+
              "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long.}"+
          "UNION 									     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat.}"+
          "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}"+
          "UNION 						   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
          "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long."+
              "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long."+
              "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
          "UNION 								     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
          "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
          "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
          "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
          "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
          "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
          "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat.}"+
          "UNION 							      {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
          "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
          "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long.}"+
          "UNION 									     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeFrom> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}"+
          "UNION 						   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long.}"+
          "UNION 									   {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
          "UNION 								     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long.}"+
          "UNION 							       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeFrom> ?lat."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
            "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long.}"+
          "UNION 						    {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_longtitudeTo> ?long."+
            "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
          "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
          "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
            "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
            "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat.}"+
          "UNION 							      {?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
            "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lat> ?lat."+
            "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
            "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
            "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>.}"+
            "UNION 	       {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
              "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
              "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
              "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLongitude> ?long."+
              "?pos <http://ces.nc3a.nato.int/ontologies/2012/04/medics#Leg_latitudeTo> ?lat.}"+
            "UNION 									 {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
              "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
              "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
              "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?long."+
              "?pos <http://www.ffi.no/semantini/ontology/threat.owl#latitude> ?lat.}"+
            "UNION 								{?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
              "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
              "?event rdf:type <http://nucessv44.ces.nc3a.nato.int/jocwatch/resource/Incident_categories/F38056CC-753B-48C5-951C-88850CE91DB1>."+
              "?unit rdf:type <http://www.ffi.no/semantini/ontology/threat.owl#Artillery>."+
              "?pos <http://www.semantini.no/NFFIsws/nffi13.owl#hasLatitude> ?lat."+
                "?pos <http://si.nc3a.nato.int/ontologies/2011/03/jocwatch#Event_Lon> ?long.}"+
            "UNION 								     {?unit <http://www.ffi.no/semantini/ontology/threat.owl#canHandle> ?event."+
              "?unit <http://www.ffi.no/semantini/ontology/threat.owl#hasPosition> ?pos."+
              "?mission <http://www.ffi.no/semantini/ontology/threat.owl#hasThreateningEvent> ?event."+
              "?pos <http://www.ffi.no/semantini/ontology/threat.owl#longitude> ?long."+
              "?pos <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.}}"


  val threat = "PREFIX  threat: <http://www.ffi.no/semantini/ontology/threat.owl#>"+
  "PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
   "PREFIX wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>"+
    "SELECT  ?mission ?unit ?lat ?long "+
  "WHERE"+
  "{ ?mission threat:hasThreateningEvent ?event ."+
  "?unit threat:canHandle ?event ."+
    "?unit threat:hasPosition ?pos ."+
    "?pos wgs84:lat ?lat ."+
    "?pos wgs84:long ?long"+
  "}"


  import federator.Distribution._
  val endpoints1 = List(Endpoint("http://localhost:3035/T1/query"),  Endpoint("http://localhost:3030/T2/query"))
  val endpoints2 = List(Endpoint("http://localhost:3030/96/query"), Endpoint("http://localhost:3035/97/query"))//, Endpoint("http://localhost:3030/75/query"), Endpoint("http://localhost:3030/98/query"))
  val endpoints3 = List(Endpoint("http://localhost:3035/T3/query"),  Endpoint("http://localhost:3030/T4/query"))
  val endpoints4 = List(Endpoint("http://sws.ifi.uio.no/sparql/gulliste"), Endpoint("http://dbpedia.org/sparql"))
  val endpoints5 = List(Endpoint("http://localhost:3035/T5/query"),  Endpoint("http://localhost:3030/T6/query"))
  val AandB = List(Endpoint("http://localhost:3035/SB/query"), Endpoint("http://localhost:3030/SA/query"))



  import federator.Heuristics._

  import utils.Preamble._
  utils.Utils.time{
    implicit val bound = 10
    val planner = new Planner //with InclusionMinimalOnly//NonSubsumedOnly
    val evaluator = new Evaluator// with IsoReduction
    val q = threat
    val proj = projects(q)
    //val results = solve_plain(q,endpoints2, planner, distributor = even,showplan=true)//JoinAlgorithms.mergeJoin _)

    //println(federator.Federation.projects)
    val results = solve(q,endpoints2, List("file://C:/Users/aud/Desktop/del-alt.txt"), planner, distributor = even,showplan=true)//JoinAlgorithms.mergeJoin _)
    display(results, proj)
  }


}