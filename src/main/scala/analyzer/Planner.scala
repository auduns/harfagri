/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package analyzer

import com.hp.hpl.jena.query.ResultSet
import com.hp.hpl.jena.sparql.engine.binding.Binding
import utils.Preamble._
import com.hp.hpl.jena.graph.{Triple => JTriple, Node}
import federator.Endpoint
import analyzer.Trees._
import federator.Querying._
import analyzer.Preflights.Preflight
import com.hp.hpl.jena.sparql.core.Var
import scala.concurrent.{Await, Future}
import concurrent.duration.Duration
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
//import no.ffi.hourglass.Preamble._

class Planner extends Preflight {

  def abst(triple: JTriple) = {
    def s(n:Node) = n.isVariable() 
    (s(triple.getSubject),s(triple.getPredicate), s(triple.getObject))
  }

  def score(triple:JTriple): Int = {
    val pattern = abst(triple)
    // Tsialiamanis ordering

    pattern match {
      case (true, true, true) => 0
      case (true, false, true) => 1
      case (false, true, true) => 2
      case (true, true, false) => 3
      case (false, false, true) => 4
      case (true, false, false) => 5
      case (false, true, false) => 6
      case _ => 7
    }
  }

  def sort(bgps: List[Set[JTriple]]) = {
    def gt(a: Set[JTriple], b:Set[JTriple]): Boolean = {
      a.forall(triple => b.exists(other => score(triple) >= score(other)))
    }
    bgps.sortWith((x, y) => gt(x, y))
  }

  /*
   Weld together to trees to form a hybrid left-leaning/bushy plan.
   The plan will be left-leaning over generic vocabulary and bushy
   over domain specific vocabulary.
    */

   def splice(llp: Tree, bush: Tree) = {
    val h = height(llp)
    def aux(llp: Tree): Tree =
      (llp: @unchecked) match {
        case tree@Join(j: Join, b: BGP) => aux(j)
        case tree@Join(a:BGP, b:BGP) =>
          val newj = Join(bush, a)
          tree.parent.asInstanceOf[Join].left = newj
          tree
      }
  }


  /**
   *
   * @param distro
   * @param project
   * @return a SORTED list of bgps, according to the Tsialiamanis ordering
   */
  def make_bgps(distro: Map[Set[JTriple], Set[Endpoint]], project:Set[Var])(implicit bound:Int) = {
    var covered: Set[Endpoint]  = Set()

    def make_bgp(triples: Set[JTriple], endpoints: List[Endpoint]) = {
      val map = ((Map.empty[Endpoint, List[Set[JTriple]]] /: endpoints){
            (map, endpoint) => if (covered contains endpoint) map
            else (map + (endpoint -> (preflight(triples, endpoint, distro))))
      })

      covered = covered union endpoints.toSet
      val bgp = BGP(triples, endpoints, map)
      bgp.projects = project
      bgp
    }
    val sorted = sort(distro.keys.toList)
    sorted map (bgp => make_bgp(bgp, distro.get(bgp).get.toList))
  }

  /**
   *
   * @param distribution
   * @param project
   * @return An executiion plan where all generic elements are given low priority
   */

  def partitioned_llp(distribution: Map[Set[JTriple], Set[Endpoint]], project: Set[Var])(implicit bound:Int) = {
      def isGenericTriple(triple: JTriple): Boolean = {
        (score(triple) == 0) || (GenericTerms.elements contains triple.getPredicate())
      }

      def isGenericCQ(cq: CQ): Boolean = {
        cq.forall(triple => isGenericTriple(triple))
      }

      val bgps: List[BGP] = make_bgps(distribution, project)

      def split(bgps: List[BGP], acc:(List[BGP], List[BGP])): (List[BGP], List[BGP]) = bgps match {
        case Nil => (acc._1.reverse, acc._2.reverse)
        case head :: tail => {
          if (isGenericCQ(head.triples)) split(tail, (acc._1, head::acc._2))
          else split(tail, (head :: acc._1, acc._2))
        }

      }

      val (domain, generic) = split(bgps, (List(), List()))
      val  tree =  (domain ::: generic).reduceLeft((b1:Tree, b2:BGP) => new  Join(b1, b2))
      localjoins(require(tree), tree.live)


  }


  def localjoins(tree: Tree, live_above: Set[Var]): Tree = (tree: @unchecked) match {
    case tree:BGP => {
      tree.joins = tree.live intersect  live_above
      tree
    }
    case tree@Join(a:Tree, b: Tree) => {
      tree.joins = tree.live intersect live_above
      localjoins(a, tree.live union live_above)
      localjoins(b, tree.live union live_above)
      tree
    }
  }

  def require(tree: Tree): Tree = {
    def occurs(v: Var, t:Tree) = variables(t.triples) contains v
    def traverse(t:Tree): Unit = (t: @unchecked) match {
      case tree@BGP(triples, _, _ ) => tree.live = variables(triples).toSet
      case tree@Join(a: Join, b: BGP) => {
        b.live = variables(b.triples).filter(
          v => (occurs(v, a))
            || (b.projects contains v)
            || (tree.live contains v)
          ).toSet
        tree.live = tree.live union b.live //a.live = b.live union tree.live
        a.live = tree.live
        traverse(a)
      }
      case tree@Join(a:BGP, b:BGP) => {
        a.live = variables(a.triples).filter(
          v => (occurs(v, b))
            || (b.projects contains v)
            || (tree.live contains v)
        ).toSet
        b.live = variables(b.triples).filter(
          v => (occurs(v, a))
            || (a.projects contains v)
            || (tree.live contains v)
        ).toSet
        tree.live = (a.live union b.live).filter(
          v => (a.projects contains v)
            || (tree.live contains v)
        )

      }
    }
    traverse(tree)
    tree
  }
}
