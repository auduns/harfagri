/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package analyzer

import federator.Querying._
import federator.Endpoint
import com.hp.hpl.jena.graph.{Triple => JTriple, Node}
import utils.Combinatorics._
import utils.Preamble._
import utils.Utils.elements


object Preflights {

  private def reduce_bgp(triples: Set[JTriple], endpoint:Endpoint): CQ = {
    val s: List[(JTriple, Boolean)] = for (triple <- triples.toList) yield {
      val constraints = variables(triple).map(v => "isBlank(" + v + ")").mkString(" || ")
      val filter = "FILTER (" + constraints + ")"
      (triple, endpoint.queryable.asked(triple, endpoint.source, filter))
    }

    val pattern: CQ = (Set[JTriple]() /: s) ((x:CQ, y:(JTriple, Boolean)) => if (y._2) x + y._1 else x)
    pattern

  }

  trait Preflight {
    def preflight(triples: Set[JTriple],endpoint: Endpoint, distro: Map[Set[JTriple], Set[Endpoint]])(implicit bound: Int): List[Set[JTriple]] = {
      val relevant_pattern: Set[JTriple] = (Set[JTriple]() /: distro) {
        case (sofar, (bgp, sources)) =>
          if (sources contains endpoint) sofar union bgp
          else sofar
      }
      val pattern = reduce_bgp(relevant_pattern, endpoint)////
      pcomponents(pattern)
    }
  }

  /**
   * Goes well with the standard distribution
   */
  trait NonSubsumedOnly extends Preflight {
    override def preflight(triples: Set[JTriple],endpoint: Endpoint, distro: Map[Set[JTriple], Set[Endpoint]])(implicit bound:Int): List[Set[JTriple]] = {
      super.preflight(triples, endpoint, distro).filterNot(p => p.subsetOf(triples))
    }
  }

  trait InclusionMinimalOnly extends Preflight {
    private def minima(s:List[CQ]) = {
      s.filter(set => !s.exists(set2 => ((set2 subsetOf set) && (set != set2))))
    }

    override def preflight(triples: Set[JTriple],endpoint: Endpoint, distro: Map[Set[JTriple], Set[Endpoint]])(implicit bound:Int): List[Set[JTriple]] = {
      minima(super.preflight(triples, endpoint, distro).filterNot(p => p.subsetOf(triples)))
    }
  }

  trait MostGeneralOnly extends Preflight {
    private def generalizesP(n1:Node, n2:Node): Boolean = {
      (n1 == n2) || ((n1.isVariable()) && ((!(n2.isVariable())) || (n1 == n2)))
    }

    private def generalizesP(t1:JTriple, t2:JTriple): Boolean = {
      val t = elements(t1).zip(elements(t2))
      (true /: t) ((bool, pair) => (bool && generalizesP(pair._1, pair._2)))
    }

    private def generalizesP(cq1:CQ, cq2:CQ): Boolean = {
      if (cq1.size < cq2.size) false
      else {
        val p = cq1.zip(cq2)
        (true/: p) ((bool, pair)=> generalizesP(pair._1, pair._2))
      }
    }

    override def preflight(triples: Set[JTriple], endpoint: Endpoint, distro: Map[Set[JTriple], Set[Endpoint]])(implicit bound:Int): List[Set[JTriple]] = {
      val temp = super.preflight(triples, endpoint, distro)
      temp.filter(set => !temp.exists(set2 => (generalizesP(set2, set) && (set != set2))))
    }
  }
}
