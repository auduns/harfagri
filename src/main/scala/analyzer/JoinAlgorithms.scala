/**
 * Hourglass: Sound and complete SPARQL federation
 * Copyright (C) 2014 Norwegian Defence Research Establishment.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package analyzer

import com.hp.hpl.jena.query.{QuerySolution => SLN}
import scala.collection.JavaConversions._
import utils.Preamble._
import com.hp.hpl.jena.sparql.engine.binding.{BindingHashMap, Binding}
import com.hp.hpl.jena.sparql.core.Var

object JoinAlgorithms {
  /**
   *
   * @param l A Jena QuerySolution
   * @param r A Jena QuerySolution
   * @param attributes A list of common attributes
   * @return true if r and l agree on common attributes, false otherwise
   */
  def joinP2(l: SLN, r: SLN,  attributes: List[String]): Boolean = {
    (true /: attributes)((x, y) => (x && (l.get(y) == r.get(y))))
  }

  /**
   *
   * @param l A Jena Binding
   * @param r A Jena Binding
   * @param attributes A list of common attributes
   * @return true if l and r agree on common attributes, false otherwise
   */
  def joinP(l: Binding, r: Binding,  attributes: List[Var]) ={
    def check(a: List[Var], res: Boolean = true): Boolean =   {
      a match {
        case Nil => res
        case head:: tail => if (l.get(head) == r.get(head)) check(tail, res) else false
      }
    }
    check(attributes)
  }

  def mergeBindings(l: Binding, r: Binding) = {
    var merged = new BindingHashMap(l)
    for (v <- r.vars().toList; if (!(merged contains v))) merged.add(v, r.get(v))
    merged
  }

  private def commonAtts(l: Binding, r: Binding): List[Var] = {
    l.vars.toList.intersect(r.vars.toList)
  }

  /**
   * Join dispatcher
   *
   * @param left A set of solutions, i.e. a list of Jena Bindings
   * @param right A set of solutions, i.e. a list of Jena Bindings
   * @param method The join algorithm
   * @return A set of solutions, i.e. a list of Jena Bindings
   */
  private def join(left: Solutions, right: Solutions)(method: (Solutions, Solutions) => Solutions): Solutions = {
    val j = if (left == right) left else{
      (left, right) match {
        case  (Nil, Nil) => Nil
        case  (Nil, right) => Nil
        case  (left, Nil) => Nil
        case  (left, right) => method(left,right).toSet.toList
      }
    }
    j

  }

  /**
   * Paralellized version of nested loop join
   *
   * @param left A set of solutions, i.e. a list of Jena Bindings
   * @param right A set of solutions, i.e. a list of Jena Bindings
   * @return A set of solutions, i.e. a list of Jena Bindings
   */
  private def parallellizedLoop(left: Solutions, right: Solutions): Solutions = {
    val atts = commonAtts(left.head, right.head)
    def findJoins(b: Binding, sols: Solutions) = for (s <- sols; if joinP(b, s, atts)) yield (b, s)
    val joins : List[(Binding, Binding)] = left.par.flatMap(b => findJoins(b, right)).toList
    joins.par.map((p: (Binding, Binding)) => mergeBindings(p._1,p._2)).toList
  }

  /**
   * Sequential version of nested loop join
   *
   * @param left A set of solutions, i.e. a list of Jena Bindings
   * @param right A set of solutions, i.e. a list of Jena Bindings
   * @return A set of solutions, i.e. a list of Jena Bindings
   */
  private def loop(left: Solutions, right: Solutions): Solutions = {
    val atts = commonAtts(left.head, right.head)
    val products = for (b1 <- left; b2 <- right; if joinP(b1, b2, atts)) yield (b1, b2)
    products.map((p: (Binding, Binding)) => mergeBindings(p._1,p._2))
  }

  /**
   * Merge-sort join
   *
   * @param _left A set of solutions, i.e. a list of Jena Bindings
   * @param _right A set of solutions, i.e. a list of Jena Bindings
   * @return A set of solutions, i.e. a list of Jena Bindings
   */
  private def merge(_left: Solutions, _right: Solutions): Solutions = {
    val atts = commonAtts(_left.head,_right.head)
    val left = _left.sortWith((x: Binding, y: Binding) => (x < y)(atts))
    val right = _right.sortWith((x: Binding, y: Binding) => (x < y)(atts))
    var result: Set[(Binding, Binding)] = Set()
    var (prr, psl) = (0,0)
    val (lsize, rsize)  = (left.size, right.size)
    while ((psl < lsize)  && (prr < rsize)) {
      var ts = left(psl)
      var Ss = Set(ts)
      psl = psl + 1
      var done = false
      while (!done && (psl < lsize)) {
        var tsprime = left(psl)
        if (joinP(tsprime, ts, atts)) {
          Ss = Ss union Set(tsprime)
          psl = psl + 1
        } else done = true
      }
      var tr = right(prr)
      while ((prr < rsize) && (tr < ts)(atts) ) {
        prr = prr + 1
        if (prr < rsize) {tr = right(prr)} else tr=tr
      }
      while ((prr < rsize) && joinP(tr, ts, atts)){
        val adds = for (tuple <- Ss) yield (tuple, tr)
        result = result union adds
        prr = prr + 1
        if (prr < rsize) {tr = right(prr)} else tr=tr
      }
    }
    result.toList.map((p: (Binding, Binding)) => mergeBindings(p._1,p._2))
  }

  def mergeJoin(left:Solutions, right:Solutions) = join(left,right)(merge)
  def nestedLoopJoin(left:Solutions, right:Solutions) = join(left,right)(loop)
  def pNestedLoopJoin(left:Solutions, right:Solutions) =  join(left,right)(parallellizedLoop)
}